import { Meteor } from "meteor/meteor";
import { Items } from "./items";

Meteor.publish("items", function() {
  return Items.find({});
});
