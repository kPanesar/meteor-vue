import HomeComponent from "/imports/ui/Home.vue";
import NotFoundComponent from "/imports/ui/NotFound.vue";
import LoginComponent from "/imports/ui/auth/Login.vue";
import RegisterComponent from "/imports/ui/auth/Register.vue";

const routes = [
  {
    path: "/",
    component: HomeComponent
  },
  {
    path: "*",
    component: NotFoundComponent
  },
  {
    path: "/login",
    component: LoginComponent
  },
  {
    path: "/register",
    component: RegisterComponent
  }
];

export default routes;
