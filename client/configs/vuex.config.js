import Vue from "vue";
import Vuex from "vuex";
import "vuetify/dist/vuetify.min.css";

Vue.use(Vuex);

import auth from "/client/store/modules/auth";

const store = new Vuex.Store({
  modules: {
    auth
  }
});

export default store;
