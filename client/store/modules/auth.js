import { Meteor } from "meteor/meteor";
import { Accounts } from "meteor/accounts-base";
import { Tracker } from "meteor/tracker";

const authModule = {
  state: {
    user: null
  },
  getters: {
    currentUser: state => {
      return state.user;
    }
  },
  mutations: {
    updateUser(state, value) {
      state.user = value;
    },
    isUsernameValid(state, value) {
      state.usUsernameValid = value;
    },
    logout(state) {
      state.user = null;
    }
  },
  actions: {
    registerUser(_, formData) {
      Accounts.createUser(formData, error => {
        console.log(error.reason);
      });
    },
    login({ commit }, formData) {
      Meteor.loginWithPassword(formData.username, formData.password, error => {
        console.log(error);
      });
      commit("updateUser", Meteor.user());
    },
    logout({ commit }) {
      Meteor.logout(() => {
        commit("logout");
        console.log("user logged out");
      });
    },
    loadUser({ commit }) {
      Tracker.autorun(() => {
        if (Meteor.user()) {
          console.log("updating user");
          commit("updateUser", Meteor.user());
        }
      });
    }
  }
};

export default authModule;
