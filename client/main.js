// Libs
import { Meteor } from "meteor/meteor";
import Vue from "vue";
import VueMeteorTracker from "vue-meteor-tracker"; // here!
Vue.use(VueMeteorTracker); // here!

// Main app
import AppComponent from "/imports/ui/App.vue";
import routerFactory from "/client/configs/router.config";
import store from "/client/configs/vuex.config";
import "/client/configs/vuetify.config";

Meteor.startup(() => {
  const router = routerFactory.create();
  new Vue({
    router,
    render: h => h(AppComponent),
    store
  }).$mount("app");
});
